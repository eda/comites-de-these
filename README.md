# Comités de these

## Deux comités, un par an en D1 et en D2

Page de référence : http://edinfomaths.universite-lyon.fr/these/suivi-de-these

## Contenus du repos

Pour chaque comité :
- le rapport d'avancement
- le planning de thèse à jour (diagramme)
- les diapos de présentation
